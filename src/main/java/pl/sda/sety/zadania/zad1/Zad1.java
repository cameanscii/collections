package pl.sda.sety.zadania.zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Zad1 {

    public static void main(String[] args) {
        Set<Integer> integerss = new HashSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));
        System.out.println(integerss);
        System.out.println(integerss.size());
        for (Integer integer:integerss){
            System.out.println(integer);
        }

        integerss.remove(10);
        integerss.remove(12);


        System.out.println(integerss);
        System.out.println(integerss.size());
        for (Integer integer:integerss){
            System.out.println(integer);
        }

        TreeSet<Integer> integerssTree = new TreeSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));
        System.out.println(integerssTree);
        System.out.println(integerssTree.size());
        for (Integer integer:integerssTree){
            System.out.println(integer);
        }

//        integerssTree.remove(10);
//        integerssTree.remove(12);
        //usuwanie jako lista
        integerss.removeAll(Arrays.asList(10,12));

        System.out.println(integerssTree);
        System.out.println(integerssTree.size());
        for (Integer integer:integerssTree){
            System.out.println(integer);
        }

    }


}
