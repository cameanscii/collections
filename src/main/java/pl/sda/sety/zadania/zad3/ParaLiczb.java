package pl.sda.sety.zadania.zad3;

    public class ParaLiczb {
        private int lewy;
        private int prawy;

        public ParaLiczb(int lewy, int prawy) {
            this.lewy = lewy;
            this.prawy = prawy;
        }

        @Override
        public String toString() {
            return "Para{" +
                    "lewy=" + lewy +
                    ", prawy=" + prawy +
                    '}';
        }
    }