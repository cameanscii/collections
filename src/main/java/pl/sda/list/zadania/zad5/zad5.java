package pl.sda.list.zadania.zad5;

import java.util.ArrayList;
import java.util.List;

public class zad5 {


    public static class Main {
        public static void main(String[] args) {
            Student pawel = new Student(12, "Pawel", "Tryniszewski", Plec.MEZCZYZNA);
            Student dawid = new Student(14, "Dawid", "rojda", Plec.MEZCZYZNA);
            Student adam = new Student(13, "Adam", "kwiatkowsk", Plec.MEZCZYZNA);
            Student mateusz = new Student(10, "mateusz", "kowalski", Plec.MEZCZYZNA);
            Student kuba = new Student(11, "kuba", "nowak", Plec.MEZCZYZNA);

            List<Student> lista = new ArrayList<>();
            lista.add(pawel);
            lista.add(adam);
            lista.add(dawid);
            lista.add(mateusz);
            lista.add(kuba);

            System.out.println(lista);

            for (Student o : lista) {
                System.out.println(o);
            }

            for (Student o : lista) {


                if (o.getPlec() == Plec.MEZCZYZNA) {
                    System.out.println(o);
                }
            }

            for (Student o : lista) {
                if (o.getPlec() == Plec.KOBIETA) {
                    System.out.println(o);
                }
            }

            for (Student o : lista) {
                System.out.println(o.getNrIndeksu());

            }
        }
    }
}

