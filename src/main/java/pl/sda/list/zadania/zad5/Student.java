package pl.sda.list.zadania.zad5;

public class Student {
    int nrIndeksu;
    String imie;
    String nazwisko;
    Plec plec;

    public Student(int nrIndeksu, String imie, String nazwisko, Plec plec) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }

    public Plec getPlec() {
        return plec;
    }

    public int getNrIndeksu() {
        return nrIndeksu;
    }

    @Override
    public String toString() {
        return nrIndeksu +" "+ imie+" "+nazwisko+" "+plec;
    }
}
