package pl.sda.list.zadania.zad6;

import java.util.ArrayList;
import java.util.List;

public class Dziennik {
    List<Student> listaUczniow= new ArrayList<>();

    public void dodajStudenta(Student student){
        listaUczniow.add(student);
    }

    public void usuńStudenta(Student student){
        listaUczniow.remove(student);
    }

    public void usuńStudenta(String nrindeksu ){
        listaUczniow.remove(nrindeksu);
    }

    public Student zwróćStudenta(String nrindexu){
        int index = listaUczniow.indexOf(nrindexu);
        return listaUczniow.get(index);
    }

//    public String podajŚredniąStudenta(Student student){
//        int suma = 0;
//        for (int i = 0; i < 10; i++) {
//            suma += student.listaOcen.get(i);
//        }
//        int srednia = suma / student.listaOcen.size();
//
//        return  toString(srednia);
//    }

}
