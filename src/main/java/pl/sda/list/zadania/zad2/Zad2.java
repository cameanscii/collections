package pl.sda.list.zadania.zad2;

import java.util.*;

public class Zad2 {
    public static void main(String[] args) {


        Random random = new Random();
        List<Integer> lista2 = new ArrayList<>();


        int suma = 0;
        for (int i = 0; i < 10; i++) {

            lista2.add(random.nextInt(12));
            suma += lista2.get(i);
        }
        int srednia = suma / lista2.size();

        System.out.println(lista2);
        System.out.println(suma);
        System.out.println(srednia);
        //kopia listy przed sortowaniem
        List<Integer> lista_kopia = new ArrayList<>(lista2);
        Collections.sort(lista2);
        System.out.println(lista2);
        int mediana;
        if (lista2.size() % 2 == 0) {
            mediana = (lista2.get(lista2.size() / 2-1) + lista2.get(lista2.size() / 2 ))/2;
        } else {
            mediana = lista2.get((lista2.size())/ 2 );
        }
        System.out.println(mediana);

        int najmniejszy = lista_kopia.get(0);
        int indexNajmniejszy=0;
        int najwiekszy=lista_kopia.get(0);
        int indexNajwiekszy=0;

        System.out.println(lista_kopia);

        for (int i = 0; i <lista_kopia.size() ; i++) {
            if (lista_kopia.get(i)>najwiekszy){
                indexNajwiekszy=i;
                najwiekszy=lista_kopia.get(i);
            }
            if (lista_kopia.get(i)<najmniejszy){
                indexNajmniejszy=i;
                najmniejszy=lista_kopia.get(i);
            }


        }
        System.out.println("Najwiekszy element to "+najwiekszy+" o indexie "+indexNajwiekszy);
        System.out.println("Najmniejszy element to "+najmniejszy+" o indexie "+indexNajmniejszy);

        System.out.println("Najwiekszy element to "+najwiekszy+" o indexie "+lista_kopia.indexOf(najwiekszy));
        System.out.println("Najmniejszy element to "+najmniejszy+" o indexie "+lista_kopia.indexOf(najmniejszy));


    }
}
