package pl.sda.list.zadania;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //ArrayList<Integer> lista=new ArrayList<>();
        //powinno byc , lepiej nowsza nomenklatura
        List<Integer> lista=new ArrayList<>();
        //dodawanie elementow
        lista.add(213);
        //dodanie elemwntu na index 0 resztasoeprzesowa
        lista.add(0,214);
        lista.set(0,215);
        //wypisanie, duzo latwiej
        System.out.println(lista);

        //tablice sa szybsze, lecz bardzo prymitywne, czasem warto stosowac
        //usuwanie
        for (int i = 0; i <lista.size() ; i++) {
            System.out.println(lista.get(i));

        }

        lista.set(0,512);
        int indexElemantu=lista.indexOf(512);
        System.out.println(indexElemantu);

    }
}
